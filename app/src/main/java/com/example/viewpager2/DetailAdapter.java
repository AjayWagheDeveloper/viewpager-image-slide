package com.example.viewpager2;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.PagerHolder> {

    Context ctx;
    ArrayList<String> dataModel;
    ArrayList<String> name;


    public DetailAdapter(Context ctx, ArrayList<String> dataModel,ArrayList<String> name) {
        this.ctx = ctx;
        this.dataModel = dataModel;
        this.name = name;
    }

    @NonNull
    @Override
    public PagerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pager_item,parent,false);
        return new PagerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PagerHolder holder, int position) {

        //String data = dataModel.get(position);

        holder.name.setText(name.get(position));

        Glide.with(ctx).load(dataModel.get(position)).into(holder.pager_image);
    }

    @Override
    public int getItemCount() {
        return dataModel.size();
    }

    public class PagerHolder extends RecyclerView.ViewHolder {

        ImageView pager_image;
        TextView name;

        public PagerHolder(@NonNull View itemView) {
            super(itemView);

            pager_image = itemView.findViewById(R.id.pager_image);
            name  = itemView.findViewById(R.id.pager_text);
        }
    }
}
