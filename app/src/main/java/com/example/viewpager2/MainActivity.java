package com.example.viewpager2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Collection;

public class MainActivity extends AppCompatActivity implements MyClickListener   {

    RecyclerView rv;
    DataAdapter dataAdapter;
    ArrayList<DataModel> data;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = findViewById(R.id.recycler_view);

       // rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setLayoutManager(new GridLayoutManager(this,3));


        String Urls[] = {"https://www.southdreamz.com/wp-content/uploads/2014/07/south-actresses-in-half-sarees-hot-pics-28-586x914.jpg","https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"
                ,"https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"
                ,"https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"

                ,"https://im.rediff.com/movies/2019/aug/06bolly4.jpg"
                ,"https://www.filmibeat.com/ph-big/2017/10/south-indian-actress-photos-in-saree_1508825432250.jpg"
                 ,"https://www.teahub.io/photos/full/228-2287405_indian-actress-wallpapers-keerthi-suresh.jpg"
                ,"https://qph.fs.quoracdn.net/main-qimg-d44af281edfc5a5924cc097d21833523"
                ,"https://cdn2.stylecraze.com/wp-content/uploads/2018/11/Sai-Pallavi.jpg"
        };

        String name[] = {
                "Tamanna Bhatia","Rajshree","Rashmika mandhana","Anu","Kajal Aggarwal","Anushka Shetty","Keerty Sursh"
                ,"Sai Pallavi","Sai Pallvii"
        };




        data = new ArrayList<>();

        for (int i=0;i < Urls.length;i++)
        {
            data.add(new DataModel(name[i],Urls[i]));
        }



//        data.add(new DataModel("Ragini Nandwani","https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna","https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel","https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel("Ragini Nandwani","https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna","https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel","https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel("Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel("Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel("Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel("Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel("Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel("Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));


        dataAdapter = new DataAdapter(this,data,this);

        rv.setAdapter(dataAdapter);

    }

//    @Override
//    public void onImageClick(DataModel dataModel) {
//
//
//
//        Toast.makeText(this, "clicked : "+ data.toString(), Toast.LENGTH_SHORT).show();
//
//        ArrayList<DataModel> mod = new ArrayList<>();
//        mod.addAll(data);
//
//        Log.d(TAG, "onImageClick: " + mod.toString());
//
//        ArrayList<MyParcel> pa = new ArrayList<>();
//        pa.addAll((Collection<? extends MyParcel>) dataModel);
//
//       MyParcel myParcel = new MyParcel();
//
//      Parcelable parcelable = Parcels.wrap(myParcel);
//
//        //Parcelable listParcelable = Parcels.wrap(mod);
//
//        Intent intent = new Intent(this,DetailPagerActivity.class);
//
//       intent.putExtra("post_data",parcelable);
//
//        startActivity(intent);
//
//    }

    @Override
    public void onImageClick(int position) {


        Toast.makeText(this, "click : "+ position, Toast.LENGTH_SHORT).show();

//        ArrayList<DataModel> mod = new ArrayList<>();
//        mod.addAll(data);

       // DataModel dataModel = mod.get(position);


//        DataModel dat = new DataModel();
//
//        dat.getId();
//        dat.getName();
//        dat.getUrl();

//        DataModel dataModel = new DataModel(data.get(position).getId(),data.get(position).getName(),
//                data.get(position).getUrl());

      // Parcelable parcelable = Parcels.wrap(dataModel);

        //Parcelable wrapped = Parcels.wrap(data);
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> images = new ArrayList<>();
        for (int i = 0; i < data.size(); i++)
        {
            images.add(data.get(i).getUrl());
            names.add(data.get(i).getName());



        }

        //images.add(data.get(position).getUrl());



        Intent intent = new Intent(this,DetailPagerActivity.class);



        intent.putExtra("post_data",images);
        intent.putExtra("post_name",names);
        intent.putExtra("position",position);
        //intent.putParcelableArrayListExtra("extra",dataModel.getId(),dataModel.getName(),dataModel.getUrl());

        startActivity(intent);
    }
}