package com.example.viewpager2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.parceler.Parcels;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DetailPagerActivity extends AppCompatActivity {

    ViewPager2 viewPager2;

    ArrayList<DataModel> data;

    DetailAdapter detailAdapter;
    private static final String TAG = "DetailPagerActivity";

    ArrayList<DataModel> mydata = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pager);

        viewPager2 = findViewById(R.id.detail_pager);


        //Bundle extras = getIntent().getExtras();

        int pos = getIntent().getExtras().getInt("position");

       // ArrayList<MyParcel>  arr = extras.getParcelableArray("post_data");

      // DataModel unwrap = Parcels.unwrap(getIntent().getExtras().getParcelable("post_data"));

       //Parcelable parcelable = getIntent().getParcelableExtra("post_data");
//
       //DataModel post_data = Parcels.unwrap(parcelable);


        //Log.d(TAG, "onCreate: "+ post_data.toString());
//
//      ArrayList<DataModel> par = new ArrayList<>();
//      par.addAll((Collection<? extends DataModel>) post_data);


     // par.add(new DataModel(post_data.getId(),post_data.getName(),post_data.getUrl()));

        //par.addAll((Collection<? extends MyParcel>) post_data);

        //String data = model.getName();

        //String data = model.getName();

       // ArrayList<DataModel> dataModels = model;


        //ArrayList<DataModel> list = new ArrayList<>(Arrays.asList(post_data));


        ArrayList<String> imageslist = (ArrayList<String>) getIntent().getSerializableExtra("post_data");
        ArrayList<String> nameslist = (ArrayList<String>) getIntent().getSerializableExtra("post_name");



        //
        data = new ArrayList<>();
        //data.addAll((Collection<? extends DataModel>) post_data);
        //data.add(new DataModel(post_data.getId(),post_data.getName(),post_data.getUrl()));

//        data.add(new DataModel(1,"Ragini Nandwani","https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(2,"Rashmika mandanna","https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(3,"Anu Emmanuel","https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel(4,"Ragini Nandwani","https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(5,"Rashmika mandanna","https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(6,"Anu Emmanuel","https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel(7,"Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(8,"Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(9,"Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel(10,"Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(11,"Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(12,"Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel(13,"Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(14,"Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(15,"Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));
//        data.add(new DataModel(16,"Ragini Nandwani", "https://www.kollywoodzone.com/data/media/5348/malayalam_actress_ragini_nandwani_new_photos_10.jpg"));
//        data.add(new DataModel(17,"Rashmika mandanna", "https://i.pinimg.com/736x/3b/48/48/3b4848b1dbe2fecfc78ff53a8009ab19.jpg"));
//        data.add(new DataModel(18,"Anu Emmanuel", "https://www.freewalldownload.com/anu-emmanuel/latest-images-of-actress-anu-emmanuel-wallpapers-download.jpg"));

        //





       Log.d(TAG, "onCreate: "+imageslist.toString());
       Log.d(TAG, "onCreate: "+nameslist.toString());

       detailAdapter = new DetailAdapter(this,imageslist,nameslist);

      viewPager2.setAdapter(detailAdapter);
      viewPager2.setCurrentItem(pos,false);

//        viewPager2.beginFakeDrag();
//        viewPager2.fakeDragBy(4f);
//        viewPager2.endFakeDrag();




    }
}