package com.example.viewpager2;

import android.os.Parcelable;

import org.parceler.Parcel;



public class DataModel {


    private String name;
    private String url;


    public DataModel(String name, String[] urls) {
    }

    public DataModel(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "DataModel{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
