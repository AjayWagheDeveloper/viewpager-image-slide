package com.example.viewpager2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;



public class DataAdapter extends RecyclerView.Adapter<DataAdapter.Myholder> {

    private ArrayList<DataModel> mydata;
    Context ctx;

    MyClickListener myClickListener;


    public DataAdapter(Context ctx,ArrayList<DataModel> mydata,MyClickListener myClickListener) {

        this.ctx = ctx;
        this.mydata = mydata;
        this.myClickListener = myClickListener;

    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid,parent,false);

        return new Myholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, int position) {

            DataModel data = mydata.get(position);

//        holder.title.setText(data.getName());


        Glide.with(ctx).load(data.getUrl()).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                myClickListener.onImageClick(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return mydata.size();
    }




    public class Myholder extends RecyclerView.ViewHolder {
        ImageView image;

        public Myholder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image_grid);



        }
    }


}
